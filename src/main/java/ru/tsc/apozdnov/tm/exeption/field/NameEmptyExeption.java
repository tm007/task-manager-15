package ru.tsc.apozdnov.tm.exeption.field;

public final class NameEmptyExeption extends AbstractFieldExeption {

    public NameEmptyExeption() {
        super("FAULT!! Name is EMPTY");
    }

}
