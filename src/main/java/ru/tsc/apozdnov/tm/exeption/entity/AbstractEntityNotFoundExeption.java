package ru.tsc.apozdnov.tm.exeption.entity;

import ru.tsc.apozdnov.tm.exeption.AbstractExeption;

public class AbstractEntityNotFoundExeption extends AbstractExeption {

    public AbstractEntityNotFoundExeption() {
    }

    public AbstractEntityNotFoundExeption(final String message) {
        super(message);
    }

    public AbstractEntityNotFoundExeption(final String message, final Throwable cause) {
        super(message, cause);
    }

    public AbstractEntityNotFoundExeption(final Throwable cause) {
        super(cause);
    }

    public AbstractEntityNotFoundExeption(final String message, final Throwable cause, final boolean enableSuppression, final boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
