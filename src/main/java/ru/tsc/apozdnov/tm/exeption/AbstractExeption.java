package ru.tsc.apozdnov.tm.exeption;

public class AbstractExeption extends RuntimeException {

    public AbstractExeption() {
        super();
    }

    public AbstractExeption(final String message) {
        super(message);
    }

    public AbstractExeption(final String message, final Throwable cause) {
        super(message, cause);
    }

    public AbstractExeption(final Throwable cause) {
        super(cause);
    }

    protected AbstractExeption(final String message, final Throwable cause, final boolean enableSuppression, final boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
