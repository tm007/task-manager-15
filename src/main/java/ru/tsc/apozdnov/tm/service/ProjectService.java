package ru.tsc.apozdnov.tm.service;

import ru.tsc.apozdnov.tm.api.repository.IProjectRepository;
import ru.tsc.apozdnov.tm.api.service.IProjectService;
import ru.tsc.apozdnov.tm.enumerated.Sort;
import ru.tsc.apozdnov.tm.enumerated.Status;
import ru.tsc.apozdnov.tm.exeption.entity.ProjectNotFoundExeption;
import ru.tsc.apozdnov.tm.exeption.field.DescriptionEmptyExeption;
import ru.tsc.apozdnov.tm.exeption.field.IdEmptyExeption;
import ru.tsc.apozdnov.tm.exeption.field.IndexIncorrectExeption;
import ru.tsc.apozdnov.tm.exeption.field.NameEmptyExeption;
import ru.tsc.apozdnov.tm.model.Project;
import ru.tsc.apozdnov.tm.model.Task;

import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

public class ProjectService implements IProjectService {

    private final IProjectRepository projectRepository;

    public ProjectService(IProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Override
    public Project findOneByIndex(Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectExeption();
        return projectRepository.findOneByIndex(index);
    }

    @Override
    public Project findOneById(String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyExeption();
        return projectRepository.findById(id);
    }

    @Override
    public Project updateByIndex(Integer index, String name, String description) {
        if (index == null || index < 0) throw new IndexIncorrectExeption();
        final Project project = findOneByIndex(index);
        if (project == null) throw new ProjectNotFoundExeption();
        project.setName(name);
        project.setDescriprion(description);
        return project;
    }

    @Override
    public Project updateById(String id, String name, String description) {
        if (id == null || id.isEmpty()) throw new IdEmptyExeption();
        if (name == null || name.isEmpty()) throw new NameEmptyExeption();
        final Project project = findOneById(id);
        if (project == null) throw new ProjectNotFoundExeption();
        project.setName(name);
        project.setDescriprion(description);
        return project;
    }

    @Override
    public Project removeByIndex(final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectExeption();
        if (index >= projectRepository.getSize()) throw new IndexIncorrectExeption();
        return projectRepository.removeByIndex(index);
    }

    @Override
    public Project removeById(String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyExeption();
        return projectRepository.removeById(id);
    }

    @Override
    public Project changeStatusById(String id, Status status) {
        if (id == null || id.isEmpty()) throw new IdEmptyExeption();
        final Project project = findOneById(id);
        if (project == null) throw new ProjectNotFoundExeption();
        project.setStatus(status);
        return project;
    }

    @Override
    public Project changeStatusByIndex(Integer index, Status status) {
        if (index == null || index < 0) throw new IndexIncorrectExeption();
        final Project project = findOneByIndex(index);
        if (project == null) throw new ProjectNotFoundExeption();
        project.setStatus(status);
        return project;
    }

    @Override
    public Project create(final String name) {
        if (name == null || name.isEmpty()) throw new NameEmptyExeption();
        return projectRepository.create(name);
    }

    @Override
    public Project create(final String name, String description) {
        if (name == null || name.isEmpty()) throw new NameEmptyExeption();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyExeption();
        return projectRepository.create(name, description);
    }

    @Override
    public Project create(final String name, final String description, final Date dateBegin, final Date dateEnd) {
        final Project project = create(name, description);
        if (project == null) throw new ProjectNotFoundExeption();
        project.setDateBegin(dateBegin);
        project.setDateEnd(dateEnd);
        return project;
    }

    @Override
    public void remove(Project project) {
        if (project == null) throw new ProjectNotFoundExeption();
        projectRepository.remove(project);
    }

    @Override
    public Project add(Project project) {
        if (project == null) throw new ProjectNotFoundExeption();
        return projectRepository.add(project);
    }

    @Override
    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    @Override
    public List<Project> findAll(final Comparator comparator) {
        if (comparator == null) return findAll();
        return projectRepository.findAll(comparator);
    }

    @Override
    public void clear() {
        projectRepository.clear();
    }

    @Override
    public List<Project> findAll(final Sort sort) {
        if (sort == null) return findAll();
        return findAll(sort.getComparator());
    }

}
