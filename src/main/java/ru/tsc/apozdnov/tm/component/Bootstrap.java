package ru.tsc.apozdnov.tm.component;

import ru.tsc.apozdnov.tm.api.controller.ICommandController;
import ru.tsc.apozdnov.tm.api.controller.IProjectController;
import ru.tsc.apozdnov.tm.api.controller.IProjectTaskController;
import ru.tsc.apozdnov.tm.api.controller.ITaskController;
import ru.tsc.apozdnov.tm.api.repository.ICommandRepository;
import ru.tsc.apozdnov.tm.api.repository.IProjectRepository;
import ru.tsc.apozdnov.tm.api.repository.ITaskRepository;
import ru.tsc.apozdnov.tm.api.service.ICommandService;
import ru.tsc.apozdnov.tm.api.service.IProjectService;
import ru.tsc.apozdnov.tm.api.service.IProjectTaskService;
import ru.tsc.apozdnov.tm.api.service.ITaskService;
import ru.tsc.apozdnov.tm.constant.ArgumentConstant;
import ru.tsc.apozdnov.tm.constant.TerminalConstant;
import ru.tsc.apozdnov.tm.controller.CommandController;
import ru.tsc.apozdnov.tm.controller.ProjectController;
import ru.tsc.apozdnov.tm.controller.ProjectTaskController;
import ru.tsc.apozdnov.tm.controller.TaskController;
import ru.tsc.apozdnov.tm.enumerated.Status;
import ru.tsc.apozdnov.tm.exeption.system.ArgumentNotSupportedExeption;
import ru.tsc.apozdnov.tm.exeption.system.CommandNotSupportedExeption;
import ru.tsc.apozdnov.tm.model.Project;
import ru.tsc.apozdnov.tm.repository.CommandRepository;
import ru.tsc.apozdnov.tm.repository.ProjectRepository;
import ru.tsc.apozdnov.tm.repository.TaskRepository;
import ru.tsc.apozdnov.tm.service.CommandService;
import ru.tsc.apozdnov.tm.service.ProjectService;
import ru.tsc.apozdnov.tm.service.ProjectTaskService;
import ru.tsc.apozdnov.tm.service.TaskService;
import ru.tsc.apozdnov.tm.util.DateUtil;
import ru.tsc.apozdnov.tm.util.TerminalUtil;

import java.util.Date;

public final class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final ITaskController taskController = new TaskController(taskService);

    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    private final IProjectTaskController projectTaskController = new ProjectTaskController(projectTaskService);

    private IProjectController projectController = new ProjectController(projectService, projectTaskService);

    public static void close() {
        System.exit(0);
    }

    private void initDemoData() {
        projectService.add(new Project("silver", "*", Status.NOT_STARTED, DateUtil.toDate("18.08.2021")));
        projectService.add(new Project("berkut", "**", Status.COMPLETED, DateUtil.toDate("13.07.2019")));
        projectService.add(new Project("global", "***", Status.IN_PROGRESS, DateUtil.toDate("07.06.1991")));
        taskService.create("TASK01", "T01");
        taskService.create("TASK02", "T02");
        taskService.create("TASK03", "T03");
    }

    public void run(final String[] args) {
        if (processArgumentTask(args)) System.exit(0);
        initDemoData();
        commandController.showWelcome();
        while (true) {
            try {
                System.out.println("Enter command:");
                final String cmd = TerminalUtil.nextLine();
                processCommandTask(cmd);
                System.out.println("****OK****");
            } catch (Exception ex) {
                System.err.println(ex.getMessage());
                System.err.println("***FAULT****");
            }
        }
    }

    public void processCommandTask(final String command) {
        if (command == null || command.isEmpty()) throw new CommandNotSupportedExeption();
        switch (command) {
            case TerminalConstant.COMMANDS:
                commandController.showCommands();
                break;
            case TerminalConstant.ARGUMENTS:
                commandController.showArguments();
                break;
            case TerminalConstant.ABOUT:
                commandController.showAbout();
                break;
            case TerminalConstant.VERSION:
                commandController.showVersion();
                break;
            case TerminalConstant.HELP:
                commandController.showHelp();
                break;
            case TerminalConstant.INFO:
                commandController.showSystemInfo();
                break;
            case TerminalConstant.TASK_CREATE:
                taskController.createTask();
                break;
            case TerminalConstant.TASK_LIST:
                taskController.showTaskList();
                break;
            case TerminalConstant.TASK_CLEAR:
                taskController.clearTasks();
                break;
            case TerminalConstant.PROJECT_CREATE:
                projectController.createProject();
                break;
            case TerminalConstant.PROJECT_LIST:
                projectController.showProjectList();
                break;
            case TerminalConstant.PROJECT_CLEAR:
                projectController.clearProjects();
                break;
            case TerminalConstant.TASK_SHOW_BY_ID:
                taskController.showTaskById();
                break;
            case TerminalConstant.TASK_SHOW_BY_INDEX:
                taskController.showTaskByIndex();
                break;
            case TerminalConstant.TASK_UPDATE_BY_ID:
                taskController.updateTaskById();
                break;
            case TerminalConstant.TASK_UPDATE_BY_INDEX:
                taskController.updateTaskByIndex();
                break;
            case TerminalConstant.TASK_REMOVE_BY_ID:
                taskController.removeTaskById();
                break;
            case TerminalConstant.TASK_REMOVE_BY_INDEX:
                taskController.removeTaskByIndex();
                break;
            case TerminalConstant.PROJECT_SHOW_BY_ID:
                projectController.showById();
                break;
            case TerminalConstant.PROJECT_SHOW_BY_INDEX:
                projectController.showByIndex();
                break;
            case TerminalConstant.PROJECT_REMOVE_BY_ID:
                projectController.removeProjectById();
                break;
            case TerminalConstant.PROJECT_REMOVE_BY_INDEX:
                projectController.removeByIndex();
                break;
            case TerminalConstant.PROJECT_UPDATE_BY_ID:
                projectController.updateById();
                break;
            case TerminalConstant.PROJECT_UPDATE_BY_INDEX:
                projectController.updateByIndex();
                break;
            case TerminalConstant.TASK_CHANGE_STATUS_BY_INDEX:
                taskController.changeTaskStatusByIndex();
                break;
            case TerminalConstant.TASK_CHANGE_STATUS_BY_ID:
                taskController.changeTaskStatusById();
                break;
            case TerminalConstant.TASK_START_BY_INDEX:
                taskController.startTaskByIndex();
                break;
            case TerminalConstant.TASK_START_BY_ID:
                taskController.startTaskById();
                break;
            case TerminalConstant.TASK_COMPLETE_BY_INDEX:
                taskController.completeTaskByIndex();
                break;
            case TerminalConstant.TASK_COMPLETE_BY_ID:
                taskController.completeTaskById();
                break;
            case TerminalConstant.PROJECT_CHANGE_STATUS_BY_ID:
                projectController.changeStatusById();
                break;
            case TerminalConstant.PROJECT_CHANGE_STATUS_BY_INDEX:
                projectController.changeStatusByIndex();
                break;
            case TerminalConstant.PROJECT_START_BY_ID:
                projectController.startById();
                break;
            case TerminalConstant.PROJECT_START_BY_INDEX:
                projectController.startByIndex();
                break;
            case TerminalConstant.PROJECT_COMPLETE_BY_ID:
                projectController.completeById();
                break;
            case TerminalConstant.PROJECT_COMPLETE_BY_INDEX:
                projectController.completeByIndex();
                break;
            case TerminalConstant.BIND_TASK_TO_PROJECT:
                projectTaskController.bindTaskToProject();
                break;
            case TerminalConstant.UNBIND_TASK_FROM_PROJECT:
                projectTaskController.unbindTaskFromProject();
                break;
            case TerminalConstant.TASK_LIST_BY_PROJECT_ID:
                taskController.showTaskListByProjectId();
                break;
            case TerminalConstant.EXIT:
                close();
                break;
            default:
                throw new CommandNotSupportedExeption(command);
        }
    }

    public void processArgumentTask(final String arg) {
        if (arg == null || arg.isEmpty()) throw new ArgumentNotSupportedExeption();
        switch (arg) {
            case ArgumentConstant.COMMANDS:
                commandController.showCommands();
                break;
            case ArgumentConstant.ARGUMENTS:
                commandController.showArguments();
                break;
            case ArgumentConstant.ABOUT:
                commandController.showAbout();
                break;
            case ArgumentConstant.VERSION:
                commandController.showVersion();
                break;
            case ArgumentConstant.INFO:
                commandController.showSystemInfo();
                break;
            case ArgumentConstant.HELP:
                commandController.showHelp();
                break;
            default:
                throw new ArgumentNotSupportedExeption(arg);
        }
    }

    public boolean processArgumentTask(final String[] args) {
        if (args == null || args.length == 0) return false;
        final String arg = args[0];
        processArgumentTask(arg);
        return true;
    }

}
