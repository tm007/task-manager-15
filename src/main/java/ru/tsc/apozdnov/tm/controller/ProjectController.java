package ru.tsc.apozdnov.tm.controller;

import ru.tsc.apozdnov.tm.api.controller.IProjectController;
import ru.tsc.apozdnov.tm.api.service.IProjectService;
import ru.tsc.apozdnov.tm.api.service.IProjectTaskService;
import ru.tsc.apozdnov.tm.enumerated.Sort;
import ru.tsc.apozdnov.tm.enumerated.Status;
import ru.tsc.apozdnov.tm.model.Project;
import ru.tsc.apozdnov.tm.util.DateUtil;
import ru.tsc.apozdnov.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class ProjectController implements IProjectController {

    private final IProjectService projectService;

    private final IProjectTaskService projectTaskService;

    public ProjectController(final IProjectService projectService, final IProjectTaskService projectTaskService) {
        this.projectService = projectService;
        this.projectTaskService = projectTaskService;
    }

    @Override
    public void showProjectList() {
        System.out.println("**** PROJECTS LIST ****");
        System.out.println("**** ENTER SORT ****");
        System.out.println(Arrays.toString(Sort.values()));
        final String sortType = TerminalUtil.nextLine();
        final Sort sort = Sort.toSort(sortType);
        final List<Project> projects = projectService.findAll(sort);
        for (Project project : projects) {
            if (project == null) continue;
            System.out.println(project);
        }
    }

    @Override
    public void clearProjects() {
        System.out.println("**** PROJECT CLEAR ****");
        projectService.clear();
    }

    @Override
    public void createProject() {
        System.out.println("**** PROJECT CREATE ****");
        System.out.println("ENTER PROJECT NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER PROJECT DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        System.out.println("ENTER DATE BEGIN: ");
        final Date dateBegin = TerminalUtil.nextDate();
        System.out.println("ENTER DATE END: ");
        final Date dateEnd = TerminalUtil.nextDate();
        projectService.create(name, description, dateBegin, dateEnd);
    }

    @Override
    public void removeByIndex() {
        System.out.println("***** REMOVE PROJECT BY INDEX ****");
        System.out.println("ENTER PROJECT INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = projectService.findOneByIndex(index);
        projectTaskService.removeProjectById(project.getId());
    }

    @Override
    public void removeProjectById() {
        System.out.println("***** REMOVE PROJECT BY ID ****");
        System.out.println("ENTER PROJECT ID:");
        final String projectId = TerminalUtil.nextLine();
        projectTaskService.removeProjectById(projectId);
    }

    @Override
    public void updateByIndex() {
        System.out.println("***** UPDATE PROJECT BY INDEX ****");
        System.out.println("ENTER PROJECT INDEX:");
        final Integer id = TerminalUtil.nextNumber() - 1;
        System.out.println("ENTER PROJECT NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER PROJECT DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        projectService.updateByIndex(id, name, description);
    }

    @Override
    public void updateById() {
        System.out.println("***** UPDATE PROJECT BY ID ****");
        System.out.println("ENTER PROJECT ID:");
        final String id = TerminalUtil.nextLine();
        System.out.println("ENTER PROJECT NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER PROJECT DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        projectService.updateById(id, name, description);
    }

    @Override
    public void showByIndex() {
        System.out.println("***** SHOW PROJECT BY INDEX ****");
        System.out.println("ENTER PROJECT INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        Project project = projectService.findOneByIndex(index);
        showProject(project);
    }

    @Override
    public void showById() {
        System.out.println("***** SHOW PROJECT BY ID ****");
        System.out.println("ENTER PROJECT ID:");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.findOneById(id);
        showProject(project);
    }

    @Override
    public void changeStatusById() {
        System.out.println("***** CHANGE PROJECT STATUS BY ID ****");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        System.out.println("ENTER STATUS:");
        System.out.println(Arrays.toString(Status.values()));
        final String statusValue = TerminalUtil.nextLine();
        final Status status = Status.toStatus(statusValue);
        projectService.changeStatusById(id, status);
    }

    @Override
    public void changeStatusByIndex() {
        System.out.println("***** CHANGE PROJECT STATUS BY INDEX ****");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("ENTER STATUS:");
        System.out.println(Arrays.toString(Status.values()));
        final String statusValue = TerminalUtil.nextLine();
        final Status status = Status.toStatus(statusValue);
        projectService.changeStatusByIndex(index, status);
    }

    @Override
    public void startById() {
        System.out.println("***** START PROJECT BY ID ****");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        projectService.changeStatusById(id, Status.IN_PROGRESS);
    }

    @Override
    public void startByIndex() {
        System.out.println("***** START PROJECT BY INDEX ****");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        projectService.changeStatusByIndex(index, Status.IN_PROGRESS);
    }

    @Override
    public void completeById() {
        System.out.println("***** COMPLETE PROJECT BY ID ****");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        projectService.changeStatusById(id, Status.COMPLETED);
    }

    @Override
    public void completeByIndex() {
        System.out.println("***** COMPLETE PROJECT BY INDEX ****");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        projectService.changeStatusByIndex(index, Status.COMPLETED);
    }

    private void showProject(final Project project) {
        if (project == null) return;
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescriprion());
        System.out.println("STATUS: " + Status.toName(project.getStatus()));
        System.out.println("CREATED: " + DateUtil.toString(project.getDateCreated()));
        System.out.println("DATE BEGIN: " + DateUtil.toString(project.getDateBegin()));
        System.out.println("DATE END: " + DateUtil.toString(project.getDateEnd()));
        System.out.println("ID: " + project.getId());
        final Status status = project.getStatus();
        if (status != null) System.out.println("STATUS: " + status.getDisplayName());
    }

}
